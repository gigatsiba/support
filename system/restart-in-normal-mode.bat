:: RESTART IN NORMAL MODE
:: https://gitlab.com/ggos/support

@echo off
setlocal ENABLEDELAYEDEXPANSION

:: CHECK FOR ADMIN PRIVILEGES
dism >nul 2>&1 || (echo This script must be Run as Administrator. && pause && exit /b 1)

:: CHANGE BOOT SETTINGS
bcdedit /deletevalue safebootalternateshell >nul 2>&1
bcdedit /deletevalue safeboot >nul 2>&1

:: RESTART
shutdown /r /d p:0:0 /c "Restarting in Normal Mode..." >nul 2>&1

exit
